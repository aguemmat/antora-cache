= Overview of The Decision Modeler

The Decision Modeler is a web application to draw and model business decision models. It is a comprehensive, user friendly and easy to use application. The Decision Modeler allows users to:


* Produce complete business decision diagrams using the latest DMN 1.2 specification,

* Create xref:_@Modelers:Decision-Modeler:decision-logic-formatting.adoc[Decision Tables] to produce desired outcomes,

* Create xref:_@Modelers:Decision-Modeler:question-and-answers.adoc[Question and Answers] inputs and outputs,

* Conduct teamwork collaborations,

* xref:_@Modelers:Decision-Modeler:decision-animator.adoc[Animate your decision models] to understand the 
logic flow.

image::overview-of-the-decision-modeler.png[]