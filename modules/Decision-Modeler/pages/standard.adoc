= Standard

The Standard view allows you to view the read-only version of the decision logic depicted to respect the DMN specification. While being in standard view mode, the results row will show for boxed context and boxed invocation logic, if it has any value.

image::standard.png[]