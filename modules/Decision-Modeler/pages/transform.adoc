= Transform

The Transform function allows you to convert your current decision logic type into an other one. In order to do that you have to click on the btn;[Transform] button.

image::transform-1.png[]

A drop down menu will appear and you will be able to select the new type of logic you want for your current decision (See the picture below).

image::transform-2.png[]