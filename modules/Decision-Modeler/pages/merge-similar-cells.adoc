= Merge Similar Cells

The merge similar cells options allows you to combine together the cells with the same content.

 
== Using the "Merge Similar Cells" option in the "Decision Table" ribbon
 
Navigate to the "Merge" panel and click on the btn;[Merge Similar] Cells" button to merge all of the cells which have the same content.

image::merge-similar-cells.png[]

This will keep merge all of the cells together which have the same content.