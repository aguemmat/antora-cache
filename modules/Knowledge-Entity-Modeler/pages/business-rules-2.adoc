= Business Rules

The "Business Rules" functionality allows you to add a new rule to your model. It's pretty simple to use, just enter a rule name in the input and then click on the plus sign or use the ENTER key.

image::business-rules-(in-business-rules).png[]