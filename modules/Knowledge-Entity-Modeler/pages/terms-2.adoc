= Terms

The "Display" functionality allows you to hide or show terms attributes such as Data type, Definition, Relations, Examples, Notes, Alternatives and Healthcare.

image::display.png[]