= Terms

The "Terms" view, display all the created terms by alphabetical order. You can edit them by clicking on a term box. You can create business vocabularies of terms with their data type, definition, examples, alternatives and notes. You can also create new terms by using the "Terms" input in the ribbon.

_Note:_ Only translated term will be displayed if you have used the xref:included/model-language.adoc[translation functionality] !

image::terms.png[]