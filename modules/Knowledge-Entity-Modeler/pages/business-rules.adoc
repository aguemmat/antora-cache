= Business Rules

The "Business Rules" view, display all the created Rules. You can edit them by clicking on them. You can rename them and enter a definition. You can also create new rules by using the "Business Rules" input in the ribbon.

image::business-rules.png[]