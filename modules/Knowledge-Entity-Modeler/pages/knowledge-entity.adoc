= Knowledge Entity

The "Knowledge Entity" ribbon allows you to access to the manage xref:included/data-type.adoc[data type] functionality.

image::knowledge-entity.png[]
  
