= Image

Capability Modeler export supports the Image file formats.

 
== Exporting into a Image format.
 
Navigate to the "Import-Export" panel and select the Image file type export. Select the type Image format you want to export into.

image::image-1.png[]

The image export function exports everything on your canvas for all your pages into a defined image file format. The exported file will be in a .zip file format called result.zip which will be prompted with a save window and downloaded to your computer. A PDF file format will not have a zip as it is exported as one file.

image::image-2.png[]

An SVG export is available under the image export. Note that the SVG export uses foreign objects containing HTML and may not work with some SVG tools.