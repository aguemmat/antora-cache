= Capability Diagram

The "Capability Diagram" function allows you to manage your HW5 elements from other models.

image::capability-diagram-1.png[]

. You can delete them:
+
image:capability-diagram-2.png[]

. You can also edit their names:
+
image:capability-diagram-3.png[]

. You can open the model where they are coming from:
+
image:capability-diagram-4.png[]