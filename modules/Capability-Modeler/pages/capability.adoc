= Capability

The "Capability" ribbon allows to manage hidden items coming from the Digital Enterprise Graph. Basically it is the reuse of HW5 elements from other models.

image::capability.png[]