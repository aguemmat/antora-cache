= Task Type

The Task type will allow you to change what kind of task is performed. When selecting a different task type, a icon will appear on the top left corner as a visual marker.

Task type applies only to the Task shape.

image::task-type-1.png[]

This will create a visual queue in the top left corner of the selected shape.

image::task-type-2.png[]


