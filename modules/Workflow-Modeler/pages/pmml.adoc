= PMML (Required: PMML Automation)

The PMML functionality allows you to manage imported PMML models.

image::pmml-1.png[]


== How to import a PMML model ?

. Navigate and click on the btn:[PMML] button in the ribbon bar.
+
image::pmml-2.png[]

. Click on the btn:[add] button.
bpmn-pmml-modal1
+
image::pmml-3.png[]

. Select an existing model in your repository or upload a new one.
+
image::pmml-4.png[]
+
image::pmml-5.png[]
+
image::pmml-6.png[]

. When your import is done, select the model.
+
image::pmml-7.png[]


== How to delete a PMML model ?

. Navigate and click on the btn:[PMML] button in the ribbon bar.

. Click on the btn:[delete] button (see the picture below).
+
image::pmml-8.png[]

 
== How to open a PMML model ?

. Navigate and click on the btn:[PMML] button in the ribbon bar.

. Click on the btn:[open] button.
+
image::pmml-9.png[]

. Then you will be able to see the content of your included file.
+
image::pmml-10.png[]


== How to rename a PMML model ?

. Navigate and click on the btn:[PMML] button in the ribbon bar.

. Click on the btn:[rename] button.
+
image::pmml-11.png[]

. Rename your file.
+
image::pmml-12.png[]
