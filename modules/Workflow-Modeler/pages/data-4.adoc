= Data

Three shapes of data are available in the Workflow Modeler: data object, data input and data output.

== Data Object

Data object exists at the process level. It is not an input nor an output but instead is internal to the process. It is used to pass data at the process level. It is transparent to the user. Moreover, data object is accessible in all embedded sub-processes.

.Data Object
image::data-(in-process-automation)-1.png[]

In the figure above, D1 is a data object. If that process is executed, end user will have no direct control over it as it is used inside the process and is not directly visible to the end user.

An important point is that when a data object is created, everywhere in the process, each data object with the same name has the same data type and the same value, so if a first task set the value of a data object, a second task coming later in the process can use another data object with the same name and in this case, access the value of the original data object.


== Data Input

.Data Input
image::data-(in-process-automation)-2.png[]

A data input is a type of data object that allows to set its value during execution of the process. It means that when a process is published, an input will be required for each data input of the process.

.Data Input Binding With a Task
image::data-(in-process-automation)-3.png[]

In the figure above, D1 is now a data input. Its value must be set by the end user of the service.


== Data output

In the figure above, D2 is a data output. D2 is an output of the process which is available to the end user.