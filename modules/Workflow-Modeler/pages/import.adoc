= Import

You can import your existing diagrams from various file formats. Workflow Modeler supports the xref:_@Modelers:Workflow-Modeler:bpmn-3.adoc[BPMN 2.0], xref:_@Modelers:Workflow-Modeler:xpdl.adoc[XPDL], xref:_@Modelers:Workflow-Modeler:project.adoc[Project], and xref:_@Modelers:Workflow-Modeler:visio.adoc[Visio] file formats. All of the import options are located in the Import-Export ribbon.

