= Create From Selection

The Sub-Process function allows you to create sub-processes from a group of selected shapes.

 
== Using the "Create From Selection" function in the "BPMN" ribbon
 
Navigate to the "BPMN Diagram" panel and click on the btn:[Create From Selection] while having 2 or more shapes selected.

image::create-from-selection.png[]

This will place the selected shapes into a new page and create a sub-process. The original shapes will be deleted from the canvas from which they were taken from.