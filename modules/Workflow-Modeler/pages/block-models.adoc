= Block Models

The block models button will block the opening of external models, on the defined shape you might have, during the process animation. The external model on a defined shape open as a new window during the animation. To continue a fluent animation without interruptions and only show the animation process, you can block those external model windows.

 
== Using the "Block Models" function in the "Process Animator" ribbon
 
Navigate to the "Play" panel and click on the btn:[Block Models] function to block any external models being opened during the animation process.

image::block-models.png[]

You can then start the animation process which will not open any external model windows.