= Simple

The simple xref:_@Modelers:Workflow-Modeler:simulate.adoc[simulation] allows for pre-defined parameters which will simulate your diagram. You can select from 3 defined question and see if and how your diagram answers those questions.

image::simple.png[]