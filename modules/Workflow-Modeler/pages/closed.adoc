= Closed

The "Closed" attribute can be set via the context menu and it's only available by right-clicking on the page and pool backgrounds.

image::closed.png[]