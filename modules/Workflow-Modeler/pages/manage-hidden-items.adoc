= Manage Hidden Items

The "Manage hidden items" function allow you to see and manage items that are not normally visible in BPMN drawing or that you have decided to hide with the option in the contextual menu.

 
== Using the "Manage hidden items" function in the "BPMN" ribbon bar:
 
Navigate to the "BPMN Diagram" panel and click on the "Manage hidden items" button.

image::manage-hidden-items-1.png[]

It will open the following panel:

image::manage-hidden-items-2.png[]