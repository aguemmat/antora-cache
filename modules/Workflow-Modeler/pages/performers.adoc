= Performers

The Performers menu allows you to specify which person is responsible for the selected task or process.

The performers menu only applies to the Task, Sub-Process and Event Sub-Process shapes.

image::performers-1.png[]

This will open a window where you will be able to define the performers new performer.

image::performers-2.png[]


== There are three types of performers:


=== User

A specified user is chosen from the existing users or an email address is typed.

 
=== Group

A group is selected from the system defined groups.

 
=== Expression

A FEEL literal expression that resolves to one or a collection of email addresses.


== The *Send Email to Performers* option allows to configure the automation behavior of this task when it becomes available:


=== Selecting Never

Selecting _Never_ will not send an email to the performers to notify them that the task is available to be performed.


=== Selecting Always

Selecting _Always_ will send an email to all performers to notify them that the task is available to be performed.


=== Selecting Only if the performer of the previous activity can't perform this activity

Selecting _Only if the performer of the previous activity_ can't perform this activity will send an email to all performers to notify them that the task is available to be performed only if the user that performed the previous task is not part of the performers of this task.


== Email Template

For automation, a custom email template can be entered.

The editor allows to enter FEEL expressions using the {;} icon. These FEEL expressions have access to the process context (can use data objects values) but also additional variables:

[cols="1,3"]
|===
|Variable Name |Description

|processName
|process name that task belongs to

|nodeName
|task name

|user
|user name (first and last name)

|formUrl
|form url to access this task
|===