= MIN/MAX Participants

The minimum/maximum participants attributes allows you to define the minimum and/or maximum number of participants in the selected shape.

The Min/Max Participants applies only to shapes which have Is Multi Instance Participant attribute selected.

image::min-max-participants-1.png[]

This will open a window where you will be able to define the min/max number of participants for the selected task.

image::min-max-participants-2.png[]