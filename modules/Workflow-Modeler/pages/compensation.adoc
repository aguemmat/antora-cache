= Compensation 

The Compensation attribute shows that the actions of the selected shape are rolled back to compensate for the activities that were performed. When enabling the compensation option, a visual marker will appear on the bottom of the shape.

The compensation applies only to the Task, Sub-Process and Event Sub-Process shapes.

image::compensation-1.png[]

This will create a visual queue on the bottom of the selected shape.

image::compensation-2.png[]