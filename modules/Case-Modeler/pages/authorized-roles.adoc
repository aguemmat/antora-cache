= Authorized Roles

The Authorized Roles option gives you the ability to edit the role of a user defined event listener.


== The Authorized Roles option only applies to Event Listener shapes which is defined as a user, Task shapes which are defined as human blocking, discretionary task shape, Collapsed and Expanded Plan Fragment shapes.
 
image::authorized-roles-1.png[]

This will open a window where you will be able to define the role options of the selected shape.

image::authorized-roles-2.png[]