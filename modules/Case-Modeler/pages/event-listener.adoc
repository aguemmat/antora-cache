= Event Listener

.User event listener
image::event-listener.png[]

Three types of event listener are available in case modelling:

* None

* *Timer:* this event listener can be set based on a timer trigger expression and/or a start trigger. A start trigger is based on a plan item, or a case file item reached state

* *User:* an authorized role can be set to limit user/group that can trigger the event listener