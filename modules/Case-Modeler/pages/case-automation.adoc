= Case Automation

The "Case Automation" section is about every supported concept in CMMN you can use in your model to publish it as a service. You will find useful information about case, event, tasks, data and how they are related to have a full executable case.