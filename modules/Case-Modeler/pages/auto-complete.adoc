= Auto Complete

The Auto Complete attribute indicates that the process requires a user to manually complete it. When enabling this option, a visual marker will appear on the bottom-middle of the shape.

 
== The Auto Complete visual marker only applies to Stage and Case Plan Model shapes.
 
image::auto-complete-1.png[]
 
This will create a visual queue on the bottom-middle of the selected shape.

image::auto-complete-2.png[]