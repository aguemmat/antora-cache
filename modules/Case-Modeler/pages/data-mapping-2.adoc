= Data Mapping

In CMMN, there is no data mapping like in BPMN. A data is made available inside a context. So, a case file item set in a case is made available throughout the case's context.

Some restrictions can be specified through the authorized roles attribute. It will restrict access to a case file item, based on roles set to read/write case file item.

.Case File Item Authorized Roles Setting Modal
image::data-mapping-(in-case-automation).png[]