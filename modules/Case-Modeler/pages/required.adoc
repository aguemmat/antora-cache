= Required

The Required attribute shows that the selected process must be completed before proceeding onto the next one. When enabling the required option, a visual marker will appear on the bottom-left of the selected shape.


== The required visual marker applies only to the Task, Stage and Milestone shapes.
 
image::required-1.png[]

This will create a visual queue on the bottom-left of the selected shape.

image::required-2.png[]