= Identifier

The identifier option allows you to add a value number to a shape to specify her order while you will export it in a report.

This identifier has to be a numeric value with dots (1, 1.6, 1.0.5.6, ...).


== How to use identifier option ?
 
. You can set the identifier of elements using the right click contextual menu under "Attributes" then clicking on "Identifier" (See the picture below).
+
image:identifier-1.png[]

. There is also a new overlay available called "Identifier" that will display the current identifier (or a dash if none is currently set) in the top left corner of the element. It will allow you also to set a new identifier.
+
image:identifier-2.png[]

. After clicking on the overlay or inside the contextual menu a window will pop up allowing you to set or modify an identifier:
+
image:identifier-3.png[]
+
If the input has green borders, it means that the identifier value you entered is correct else not. If the value is not correct she will not be set! (See the picture below)
+
image:identifier-4.png[]

You can manage this xref:included/overlays.adoc[overlay] like the others in the right side panel.