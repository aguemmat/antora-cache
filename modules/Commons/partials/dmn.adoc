= DMN

The DMN function allows you to xref:page$included/link-unlink-to-existing-decision.adoc[link to an existing Decision, unlink] the decision from the task or xref:page$included/open-decision.adoc[open directly] in the Decision Modeler the selected decision.

image::_@Modelers:Commons:image$dmn.png[]