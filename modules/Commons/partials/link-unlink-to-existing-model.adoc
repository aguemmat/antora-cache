= Link/Unlink to Existing Case Plan Model

Link/unlink to an existing Case Plan Model allows you to choose, from the repository, a CMMN file that will be linked to the selected process.

 
== Using the "Link/unlink to Existing Case Plan Model" function in the "BPMN" ribbon.
 
While having a Case task selected, navigate to the btn:[Reuse] panel, click on CMMN and selected "Link to Existing Case Plan Model". You can also unlink the case plan model by clicking on the Unlink function.

image::_@Modelers:Commons:image$link-unlink-to-existing-case-plan-model-1.png[]

This will open a file selection window where you will be able to chose which case plan model file to link.

image::_@Modelers:Commons:image$link-unlink-to-existing-case-plan-model-2.png[]

Click on the btn:[folder] icon on the right to open the repository to select the case plan model file.

image::_@Modelers:Commons:image$link-unlink-to-existing-case-plan-model-3.png[]

Once the selection has been made, select the case plan model from the dropdown menu and press Ok. This will link your selected process to the case plan model.

image::_@Modelers:Commons:image$link-unlink-to-existing-case-plan-model-4.png[]