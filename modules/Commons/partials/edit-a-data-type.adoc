= Edit a Data Type

. Click on the "pencil" button at the right of the type you wanna edit.

image::_@Modelers:Commons:image$edit-a-data-type.png[]

. Then you can rename the type or change it's structure.

. In order to finish, save your modifications by clicking on btn:[save].