= Color

The "Color" function allows you to change the color of the text label used to describe or define a shape(s).

 
== Using the "Color" function from the "Home" ribbon
 

Navigate to the "Font" panel and click on the tn[Color] button to change the color of the label text.

image::_@Modelers:Commons:image$color-1.png[]

Click on the color you want your label text to be colored with.

image::_@Modelers:Commons:image$color-2.png[]