= Attributes

The attributes are options which can be selected via check-mark. These options will always provide a visual queue on the selected shape and vary depending on the selected shape. You can also set them as visible or not for some available shapes.

image::_@Modelers:Commons:image$attributes.png[]