= Select a Data Type

There are a numbers of way to change the data type of an element:

image::_@Modelers:Commons:image$select-a-data-type.png[]

== Simple type

Select from one of the standard FEEL types

 
== Existing Type

Display a window with the data types that are already used by this model. This list can be searched.

 
== Structure

Create a new complex data type structure inline.

 

== Reuse from graph (Required: Digital Enteprise Graph)

Allows to pick a data type already defined in another model from the Digital Enterprise Graph.


== Reuse from accelerator (Required: Digital Enteprise Graph)

Allows to pick a data type already defined in an accelerator.