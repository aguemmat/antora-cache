= File

The "File" menu allows to work with the basic functions of Discovery Accelerator.

From this menu, you can create a xref:page$included/new.adoc[New], xref:page$included/open.adoc[Open], xref:page$included/save.adoc[Save] and xref:page$included/print.adoc[Print] a project. Also you can send a xref:page$included/feedback.adoc[Feedback] report, access the xref:page$included/about.adoc[About] section of the tool and get help on the functionalities of Discovery Accelerator.

image::_@Modelers:Commons:image$file.png[]



