= Semantic Link

The semantic link tab allows you to create a semantic reference for your shapes. You can add one or several links for the selected shape. These links come from various framework references which can be user defined.

 
== The semantic links apply to all of the shapes.
 
The semantic link tab allows you will be able to add, edit and delete the semantic links.

image::_@Modelers:Commons:image$semantic-link-1.png[]

Selected the framework you wish to add and click on the btn:[selection] button image:_@Modelers:Commons:image$semantic-link-icon.png[] to open the selected framework. Select the framework you wish to add in the list and click close.

You can also use the search form.

image::_@Modelers:Commons:image$semantic-link-2.png[]

Click the add button and the selected framework will be added to the shape you are working on. You can also edit, view or delete the current or other added frameworks that are available in the shape.

image::_@Modelers:Commons:image$semantic-link-3.png[]

After you add your semantic link it will be listed in the panel (see the picture below). If you add a semantic link from the Digital Enterprise Graph you have the possibility to access the linked element.

image::_@Modelers:Commons:image$semantic-link-4.png[]

In order to do that, navigate to the model name and click on it. It will open the linked model in the right modeler. It works also in read only mode.

image::_@Modelers:Commons:image$semantic-link-5.png[]



