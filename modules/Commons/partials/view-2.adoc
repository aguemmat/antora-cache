= View

The "View" panel allows you to switch to a Relation view to visualize the relationships in your BPMN model and the Diagram view.

image::_@Modelers:Commons:image$view-2-1.png[]

The Relation view is editable so you can create or remove relationship from that view. You can also quickly highlight the elements without relationships from that view.

The picture below show an example of a Relation view from a BPMN diagram:

image::_@Modelers:Commons:image$view-2-2.png[]

_Note_: It's possible also to print the relation view