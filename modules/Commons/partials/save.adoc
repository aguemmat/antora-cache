= Save

There are several ways to save a diagram within the Knowledge Entity Modeler. You can use the btn:[Save] and btn:[Save As] options in the "File" menu, quick save via the btn:[Save] icon or use the save shortcut btn:[CTRL+S].

The quick save icon is located on the top left menu, above the ribbons of the Knowledge Entity Modeler.

image::_@Modelers:Commons:image$save-1.png[]

== Using the "Save" in the "File" menu
 
Navigate to the "File" menu and click on the btn:[Save] option.

image::_@Modelers:Commons:image$save-2.png[]

This will save the changes of your current diagram within your Private Cloud Repository.