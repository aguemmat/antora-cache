= Data

The data section allows you to manage the data types available in your model and create new ones. Data types are data sets that can be defined to be pre-filed and used with Case File Items.