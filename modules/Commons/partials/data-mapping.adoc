= Data Mapping

The Data  Mapping allows you to define the data transformation for Data Associations connected to the selected shape with her inner data inputs and data outputs. Its available for all task types and events shapes.


== How to use this functionality ?

. Navigate to the task and click on the "Data Mapping" in the attributes context menu:
+
image::_@Modelers:Commons:image$data-mapping-1.png[]

. Then the following modal will appear.
+
image::_@Modelers:Commons:image$data-mapping-2.png[]