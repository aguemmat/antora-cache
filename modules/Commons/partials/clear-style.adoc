= Clear Style


The "Clear style" option allows you to clear all style effects of the text from the selected shape(s).

 
== Using the "Clear style" function from the "Home" ribbon:
 
Navigate to the font panel and click on the btn:[Clear] button (the see picture below).

image::_@Modelers:Commons:image$clear-style.png[]