= HTML

== Exporting into HTML format

Navigate to the "Export" panel and click on the btn:[HTML] button

image::_@Modelers:Commons:image$html-1.png[]

A report generation window will open where you will be able to select what you want to be included in the generated report.

Note that exporting using the By Identifier ordering will not output elements that have no Identifier defined !

image::_@Modelers:Commons:image$html-2.png[]

The HTML report will create a cover page for your project as well as the sections of the report. The sections of the report will be a picture of the diagram(s) followed by the shapes in the said picture of the diagram(s) and the documentation entered for each shape if present.